# ! pip install prettytable
# ! pip install python-telegram-bot==13.14

from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext, CallbackQueryHandler
from telegram.parsemode import ParseMode

import json
import datetime
import prettytable as pt

updater = Updater("6688123241:AAHy_mJXAru6lPf_cKSKjjVoJbWtlZPI7n8", use_context=True)  # Токен API к Telegram
dispatcher = updater.dispatcher

path_users = 'dbs/users.json'
path_slots = 'dbs/slots.json'
path_schedule = 'dbs/schedule.json'

admins = ['181447038', '1490376629']


# определяем год
year = datetime.datetime.now().year
start =  datetime.date(year, 1, 1)
end = datetime.date(year, 12, 31)
current = start
dates = []
while current <= end:
    dates.append(current)
    current += datetime.timedelta(days=1)

weekdays = []

mondays =[]
tuesdays =[]
wednesdays =[]
thursdays =[]
fridays =[]
saturdays =[]
sundays =[]

# lists pf weekdays
for day in dates:
    cur_day = day.weekday()

    if cur_day == 0:
        mondays.append(day)
    elif cur_day == 1:
        tuesdays.append(day)
    elif cur_day == 2:
        wednesdays.append(day)
    elif cur_day == 3:
        thursdays.append(day)
    elif cur_day == 4:
        fridays.append(day)
    elif cur_day == 5:
        saturdays.append(day)
    elif cur_day == 6:
        sundays.append(day)

weekdays.append(mondays)
weekdays.append(tuesdays)
weekdays.append(wednesdays)
weekdays.append(thursdays)
weekdays.append(fridays)
weekdays.append(saturdays)
weekdays.append(sundays)

week_names = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье']
week_names_u = ['понедельник', 'вторник', 'среду', 'четверг', 'пятницу', 'субботу', 'воскресенье']


def add_slots(weekday:int, start_time:str, duration:int, place:str, capacity:int):
    with open(path_schedule, 'r') as file:
        list_schedule = json.load(file)
    
    # последний id
    new_id = max([item['id'] for item in list_schedule]) + 1

    # добавить запись в schedule.json
    list_schedule.append({
        'id': new_id,
        'weekday': weekday, 
        'start_time': start_time, 
        'duration': duration, 
        'place': place,
        "capacity" : capacity
        })

    # записать новую БД в файл 
    with open(path_schedule, 'w') as f:
        json.dump(list_schedule, f)

    # добавить слотов в slots.json до конца года
    with open(path_slots, 'r') as file:
        list_of_slots = json.load(file)
    
    days = weekdays[weekday]
    for day in days:
        new_slot_id = max([item['id'] for item in list_of_slots]) + 1

        hours = start_time.split(":")[0]
        mins = start_time.split(":")[1]
        start_time_datetime = datetime.datetime.combine(day, datetime.time(int(hours), int(mins)))

        new_row = {
            'id': new_slot_id,
            'schedule': new_id,
            'capacity': capacity,
            'available': True,
            'datetime': str(start_time_datetime),
            "current_capacity" : 0
        }
        list_of_slots.append(new_row)

    # записать новую БД в файл 
    with open(path_slots, 'w') as f:
        json.dump(list_of_slots, f)


def get_available_slots():
    with open(path_slots, 'r') as file:
        list_of_slots = json.load(file)
    now = datetime.datetime.now()
    
    list_of_nearest = [slot for slot in list_of_slots if (datetime.datetime.strptime(slot['datetime'], '%Y-%m-%d %H:%M:%S') > now)][:6]

    return list_of_nearest

def slotsCommand(update, context):
    current_tid = str(update.message.from_user.id)
    buttons = []
    
    # подгружаем список всех пользователей и всех слотов
    # with open(path_users, 'r') as file:
    #     list_of_users = json.load(file)
    # подгружаем список всех пользователей и всех слотов
    with open(path_schedule, 'r') as file:
        list_schedule = json.load(file)

    # показать доступные слоты
    nearest_slots = get_available_slots()

    for slot in nearest_slots:
        
        # находим нужный шедул
        schedule = [sch for sch in list_schedule if sch["id"] == slot['schedule']][0]

        text = f'{slot["datetime"][:-2]} {schedule["place"]}'
        slot_id = slot['id']
        call_back = f'choice_slot|{str(slot_id)}|'

        button = [{"text": text, "callback_data": call_back},]
                
        buttons.append(button)

    keyboard = {"inline_keyboard": buttons}
    dispatcher.bot.send_message(chat_id=current_tid, text='Доступные слоты для записи:', parse_mode=ParseMode.HTML, reply_markup=keyboard)
    
def make_phone_buttons(callb, callb_3):
    buttons = [[
                {"text": "1","callback_data": f'{callb}|1|{callb_3}'},
                {"text": "2","callback_data": f'{callb}|2|{callb_3}'},
                {"text": "3","callback_data": f'{callb}|3|{callb_3}'},],[
                {"text": "4","callback_data": f'{callb}|4|{callb_3}'},
                {"text": "5","callback_data": f'{callb}|5|{callb_3}'},
                {"text": "6","callback_data": f'{callb}|6|{callb_3}'},],[
                {"text": "7","callback_data": f'{callb}|7|{callb_3}'},
                {"text": "8","callback_data": f'{callb}|8|{callb_3}'},
                {"text": "9","callback_data": f'{callb}|9|{callb_3}'},],[
                {"text": "0","callback_data": f'{callb}|0|{callb_3}'},
                ]]
    return buttons

def buttons(update, context):
    current_tid = update.effective_user.id
    callback = update.callback_query # callback
    callback_1, callback_2, callback_3 = callback.data.split('|')

    with open(path_slots, 'r') as file:
        list_of_slots = json.load(file)
    with open(path_schedule, 'r') as file:
        list_schedule = json.load(file)
    with open(path_users, 'r') as file:
        list_of_users = json.load(file)

    if callback_1 == 'cancel_rent': # возврат к выбору слотов юзерами (cancel)
        buttons = []
        # показать доступные слоты
        nearest_slots = get_available_slots()
        with open(path_schedule, 'r') as file:
            list_schedule = json.load(file)

        for slot in nearest_slots:
            
            # находим нужный шедул
            schedule = [sch for sch in list_schedule if sch["id"] == slot['schedule']][0]

            text = f'{slot["datetime"][:-2]} {schedule["place"]}'
            slot_id = slot['id']
            call_back = f'choice_slot|{str(slot_id)}|'

            button = [{"text": text, "callback_data": call_back},]
                    
            buttons.append(button)

        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text='Доступные слоты для записи:', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'rent': # юзер записывается в слот
        # проверяем номер телефона
        usr = [user for user in list_of_users if user['id'] == str(current_tid)][0]
        if not usr['phone_number']: # спрашиваем телефон
            text = 'Чтобы записаться - необходимо добавить свой контактный номер телефона\nДля этого введите команду /phone пробел номер телефона для связи или наберите цифры с помощью клавиатуры'
            callback.edit_message_text(text=text, parse_mode=ParseMode.HTML,) # reply_markup=keyboard)

        else: # подтверждаем бронь
            text = 'Ваша заявка принята, пожалуйста оплатите игру переводом по телефону +7 968 605 5825'
            callback.edit_message_text(text=text, parse_mode=ParseMode.HTML,) # reply_markup=keyboard)
            slot_info = [sl for sl in list_of_slots if sl["id"] == int(callback_2)][0]
            schedule_info = [sch for sch in list_schedule if sch["id"] == slot_info["schedule"]][0]

            # ОТправить сообщение Саше за подтверждением
            buttons = [[
                {"text": "подтверждаем","callback_data": f'aproove_rent|{str(slot_info["id"])}|{usr["id"]}'},
                {"text": "отмена","callback_data": f'drop_rent||{usr["id"]}'},
                ]]
            keyboard = {"inline_keyboard": buttons}
            text_sasha = f'Заявка:\nклиент {usr["username"]}\n{usr["last_name"]}\n{usr["first_name"]}\n{usr["phone_number"]}\nхочет забронировать {slot_info["datetime"]} {schedule_info["place"]}'
            dispatcher.bot.send_message(chat_id=admins[0], text=text_sasha, parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'drop_rent':
        dispatcher.bot.send_message(chat_id=admins[0],text='отклонена', parse_mode=ParseMode.HTML,)
        dispatcher.bot.send_message(chat_id=callback_3, text='Ваша заявка отклонена(((', parse_mode=ParseMode.HTML,)

    elif callback_1 == 'aproove_rent':
        # записать capacity
        with open(path_slots, 'r') as file:
            list_of_slots = json.load(file)
        for slot in list_of_slots:
            if slot["id"] == int(callback_2):
                slot['current_capacity']+= 1
        with open(path_slots, 'w') as f:
            json.dump(list_of_slots, f)
        # записать rents
        
        buttons = [[
                {"text": "","callback_data": f'||'},
                ]]
        # отправить сообщение юзеру
        dispatcher.bot.send_message(chat_id=admins[0],text='подтверждена', parse_mode=ParseMode.HTML,)
        dispatcher.bot.send_message(chat_id=callback_3, text='Ваша заявка подтверждена!', parse_mode=ParseMode.HTML,)

    elif callback_1 == 'choice_slot': # Если юзер выбрал слот
        slot_id = callback_2 # str
        rent_slot = [slot for slot in list_of_slots if slot['id'] == int(slot_id)][0]
        pl = [sch for sch in list_schedule if sch['id'] == rent_slot['schedule']][0]
        wnu = week_names_u[pl['weekday']]

        # Если ёмкость позволяет
        if rent_slot["current_capacity"] <= rent_slot["capacity"]: 
            # кнопка записаться 
            buttons = [[
                {"text": "запись","callback_data": f'rent|{slot_id}|'},
                {"text": "отмена","callback_data": f'cancel_rent||'},
                ]]
            keyboard = {"inline_keyboard": buttons}
            text = f'Записываемся на {wnu} \n{rent_slot["datetime"][:-2]}\n место {pl["place"]} '
            callback.edit_message_text(text=text, parse_mode=ParseMode.HTML, reply_markup=keyboard)
        
        # Если места закончились
        else:
            buttons = [[
                {"text": "отмена","callback_data": f'cancel_rent||'},
                ]]
            keyboard = {"inline_keyboard": buttons}
            callback.edit_message_text(text='На данный слот места закончились(', parse_mode=ParseMode.HTML, reply_markup=keyboard)


        # кнопка назад к выбору слотов
        # button = [{"text": text, "callback_data": call_back},]
        # buttons.append()

    elif callback_1 == 'choice_schedule': # Если админ выбрал шедул
        schedule_id = callback_2 # str
        # кнопка назад к выбору шедулов
    
    
    elif callback_1 == 'cancel_schedule': # Если cansel schedule

        if current_tid in admins:
            buttons = []

            # все шедулы
            with open(path_schedule, 'r') as file:
                list_schedule = json.load(file)
            
            for schedule in list_schedule:
                id_schedule = schedule['id']
                call_back = f'choice_schedule|{id_schedule}|'
                weekday = week_names[schedule['weekday']]
                start = schedule['start_time']
                place = schedule['place']
                text = f'{weekday} {start} {place}'

                button = [{"text": text, "callback_data": call_back},]
                
                buttons.append(button)

            keyboard = {"inline_keyboard": buttons}
            callback.edit_message_text(text='текущие расписания', parse_mode=ParseMode.HTML, reply_markup=keyboard)



    elif callback_1 == 'phone1':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone2', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone2':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone3', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone3':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone4', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone4':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone5', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone5':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone6', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone6':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone7', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone7':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone8', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone8':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone9', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone9':
        callback_3 += callback_2
        buttons = make_phone_buttons('phone_full', callback_3)
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'{callback_3}\nВведите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    # elif callback_1 == 'phone10':
    #     callback_3 += callback_2
    #     buttons = make_phone_buttons('phone_full', callback_3)
    #     keyboard = {"inline_keyboard": buttons}
    #     callback.edit_message_text(text='Введите цифру', parse_mode=ParseMode.HTML, reply_markup=keyboard)
    
    elif callback_1 == 'phone_full':
        callback_3 += callback_2
        # buttons = make_phone_buttons('phone4', callback_3)
        buttons = [[
                {"text": "подтвердить","callback_data": f'phone_ok||{callback_3}'},
                {"text": "отмена","callback_data": f'phone_cancel||'},
                ]]
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text=f'Ваш номер {callback_3}?', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone_cancel':
        buttons = make_phone_buttons('phone1', '')
        keyboard = {"inline_keyboard": buttons}
        callback.edit_message_text(text='Введите Ваш номер по одной цифре, начиная с 9', parse_mode=ParseMode.HTML, reply_markup=keyboard)

    elif callback_1 == 'phone_ok':
        with open(path_users, 'r') as file:
            list_of_users = json.load(file)
        # print(current_tid)
        for usr in list_of_users:
            if usr['id'] == str(current_tid):
                # print(current_tid)
                usr["phone_number"] = f'+7{callback_3}'
                call_back = f'cancel_rent||'
                button = [[{"text": 'выбрать слот', "callback_data": call_back},]]
                keyboard = {"inline_keyboard": button}
                callback.edit_message_text(text='Спасибо, номер добавлен, теперь можно выбрать слоты', parse_mode=ParseMode.HTML, reply_markup=keyboard)
                
                with open(path_users, 'w') as f:
                    json.dump(list_of_users, f)
                break


def phoneCommand(update, context):
    caption = update.message.text[7:]
    tid = str(update.message.from_user.id)

    if not caption:
        buttons = make_phone_buttons('phone1', '')
        keyboard = {"inline_keyboard": buttons}
        dispatcher.bot.send_message(chat_id=tid, text='Введите Ваш номер по одной цифре, начиная с 9', parse_mode=ParseMode.HTML, reply_markup=keyboard)
    
    else:
        with open(path_users, 'r') as file:
            list_of_users = json.load(file)
        for usr in list_of_users:
            if usr['id'] == tid:
                usr["phone_number"] = f'+7{caption}'
                call_back = f'cancel_rent||'
                button = [[{"text": 'выбрать слот', "callback_data": call_back},]]
                keyboard = {"inline_keyboard": button}
                dispatcher.bot.send_message(chat_id=tid, text='Спасибо, номер добавлен, теперь можно выбрать слоты', parse_mode=ParseMode.HTML, reply_markup=keyboard)
                
                with open(path_users, 'w') as f:
                    json.dump(list_of_users, f)
                break
    
def showScheduleCommand(update, context): # админ
    # global admins
    admin = str(update.message.from_user.id)
    if admin in admins:
        buttons = []

        # все шедулы
        with open(path_schedule, 'r') as file:
            list_schedule = json.load(file)
        
        for schedule in list_schedule:
            id_schedule = schedule['id']
            call_back = f'choice_schedule|{id_schedule}|'
            weekday = week_names[schedule['weekday']]
            start = schedule['start_time']
            place = schedule['place']
            text = f'{weekday} {start} {place}'

            button = [{"text": text, "callback_data": call_back},]
            
            buttons.append(button)

        keyboard = {"inline_keyboard": buttons}
        dispatcher.bot.send_message(chat_id=admin, text='текущие расписания', parse_mode=ParseMode.HTML, reply_markup=keyboard)
    
def startCommand(update: Update, context: CallbackContext):
    update.message.reply_text(
        f'Футбол в Москве. \nбронируем слоты для игр в футбол'
        )

    # достать файл с юзерами
    # list_of_users = mongo_get_data('users')
    with open(path_users, 'r') as file:
        list_of_users = json.load(file)
    
    # Проверить на уникальность
    if not str(update.message.from_user.id) in [user['id'] for user in list_of_users]:
        
        # добавить юзера в БД
        list_of_users.append({
            'id':str(update.message.from_user.id), 
            'username':update.message.from_user.username, 
            'last_name':update.message.from_user.last_name, 
            'first_name':update.message.from_user.first_name,
            'phone_number': None
            })

        # записать новую БД в файл 
        with open(path_users, 'w') as f:
            json.dump(list_of_users, f)

        print(f'NEW user start {update.message.from_user.username} {update.message.from_user.id}')

    else:
        print(f'OLD user start {update.message.from_user.username} {update.message.from_user.id}')

dispatcher.add_handler(CommandHandler("start", startCommand))
dispatcher.add_handler(CommandHandler('showSchedule', showScheduleCommand)) # admin
# вычеркнуть юзера из слота
# посмотреть юзеров в слоте
# Подтвердить бронь (оплату)
# закрыть слот для записи 
dispatcher.add_handler(CommandHandler('slots', slotsCommand))
dispatcher.add_handler(CommandHandler('phone', phoneCommand))
dispatcher.add_handler(CallbackQueryHandler(buttons))

updater.start_polling()
updater.idle()
